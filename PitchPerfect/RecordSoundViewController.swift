//
//  RecordSoundViewController.swift
//  PitchPerfect
//
//  Created by mukti on 07/03/19.
//  Copyright © 2019 Mukti Wibowo. All rights reserved.
//

import UIKit
import AVFoundation

class RecordSoundViewController: UIViewController, AVAudioRecorderDelegate {
    
                    var audioRecorder:      AVAudioRecorder!
    @IBOutlet weak  var messageRecording:   UIButton!
    @IBOutlet weak  var messageRecorded:    UIButton!
    @IBOutlet weak  var messageDisplay:     UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        messageRecorded.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func recordSound(){
        messageDisplay.text         = "Recording..."
        messageRecording.isEnabled  = false
        messageRecorded.isEnabled   = true
        
        let dirPath         = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let audioName       = "recordedVoice.wav"
        let pathArray       = [dirPath, audioName]
        let filePath        = URL(string: pathArray.joined(separator: "/"))
        
        let session         = AVAudioSession.sharedInstance()
        try! session.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.default, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
        
        try! audioRecorder              = AVAudioRecorder(url: filePath!, settings: [:])
        audioRecorder.delegate          = self
        audioRecorder.isMeteringEnabled = true
        audioRecorder.prepareToRecord()
        audioRecorder.record()
    }
    
    @IBAction func stopRecording(){
        messageDisplay.text         = "Tap to record"
        messageRecording.isEnabled  = true
        messageRecorded.isEnabled   = false
        
        audioRecorder.stop()
        let audioSession            = AVAudioSession.sharedInstance()
        try! audioSession.setActive(false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sendAudioRecorder" {
            let playSoundVoice  = segue.destination as! PlaySoundViewController
            let recordedAudio   = sender as! URL
            playSoundVoice.recordAudioUrl = recordedAudio
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if flag {
            performSegue(withIdentifier: "sendAudioRecorded", sender: audioRecorder.url)
        }else{
            print("Not successfully recorded")
        }
    }
}

